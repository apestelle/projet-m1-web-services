# Looping

Projet ayant pour but d'utiliser les APIs de Gitlab afin de lister les projets d'un utilisateurs et ses pipelines. On pourra mettre en favori les projets, et faire pleins de trucs interessants.

**Sonarqube:** http://sonar.galactae.eu/dashboard?id=elanis%3Aprojet-m1-web-services  
**Trello:** https://trello.com/b/sZTVkAwW/webservicem1  

# APIs

## Appels REST

**GET:**
- https://gitlab.com/api/v4/projects/{projectId}/members/all : Recuperer les membres d'un projet
- https://gitlab.com/api/v4/projects/{projectId}/pipelines?per_page=100&page={page} : Recuperer les pipelines d'un projet
- https://gitlab.com/api/v4/users/{username}/projects?per_page=100&page={page} : Recuperer les projets d'un utilisateur
- https://gitlab.com/api/v4/projects/{id : Recuperer les informations d'un projet
- https://gitlab.com/api/v4/projects?per_page=25&page={page} : Recuperer les 25 derniers projets publics créés
- https://gitlab.com/api/v4/search?scope=projects&search={searchTerms} : Rechercher dans les projets (public ou auxquels on a accès)

**POST:**
- https://gitlab.com/api/v4/projects/{projectId}/pipeline?ref=master : Lancer une nouvelle integration continue (pipeline)
- https://gitlab.com/api/v4/projects/{projectId}/pipelines/{pipelineId}/retry : Reessayer une pipeline
- https://gitlab.com/api/v4/projects/{projectId}/pipelines/{pipelineId}/cancel : Annuler une pipeline en cours

**PUT:**  
- https://gitlab.com/api/v4/projects/{projectId} - Body: { name: 'newName' } : Editer un projet, ici utilisé pour renommer

**DELETE:**
- https://gitlab.com/api/v4/projects/{projectId}/pipelines/{pipelineId} : Supprimer une pipeline de l'historique

## Appels GraphQL

- https://gitlab.com/api/graphql : Informations de l'utilsateur courant
> query {currentUser {name, username, avatarUrl}}


## Expositions

**GET:**
- **api/favorites** : Récuperer la liste complète des favoris de l'utilisateur
- **api/favoritesIds** : Récuperer la liste complète des IDs des favoris de l'utilisateur

**POST:**
- **api/favorites/create** : Ajouter un favori a la liste de l'utilisateur courant

**PUT:**
- **api/favorites/updateFavoriteRank** : Modifier l'ordre de préference des favoris de l'utilisateur courant

**DELETE:**
- **api/favorites/delete/{id}** : Supprimer un favori de la liste de l'utilisateur courant


**NB:** Pour les APIs exposées les valeurs passées sont bindées dans un FormData (pour envoyer le body)