﻿using Looping.Common;
using Looping.DataAccess.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Looping.DataAccess.Implementations
{
	public class MyDataAccess : IMyDataAccess {
		private readonly string SqlConnectionString;
		private readonly IHttpContextAccessor _httpContextAccessor;
		private string _userName;
		public MyDataAccess(IOptions<AppSettings> appSettings,
							IHttpContextAccessor httpContextAccessor)
		{
			if (appSettings != null && appSettings.Value != null)
				SqlConnectionString = appSettings.Value.SqlConnectionString;

			_httpContextAccessor = httpContextAccessor;

			_userName = _httpContextAccessor.HttpContext.User.Identity.Name;
		}

		public async Task<JArray> GetAll()
		{
			CheckFileExistence();
			return JArray.FromObject(GetJsonDataValues()["Data"]);
		}

		public async Task<Dictionary<string, string>> CreateFavorite(Dictionary<string, string> favorite)
		{
			CheckFileExistence();

			var currentJsonData = GetJsonDataValues();
			
			favorite.Add("rank", (currentJsonData["Data"].Count() + 1).ToString());
			
			currentJsonData["Data"] = JToken.FromObject(JArray.FromObject(currentJsonData["Data"]).Append(JObject.FromObject(favorite)));

			using (StreamWriter sw = new StreamWriter(Directory.GetCurrentDirectory() + "\\data." + _userName + ".json"))
			{
				sw.WriteLine(JsonConvert.SerializeObject(currentJsonData));
			}
			return favorite;
		}
		public async Task<int[]> GetFavoriteIds()
		{
			CheckFileExistence();
			var currentJsonData = JArray.FromObject(GetJsonDataValues()["Data"]);
			return currentJsonData.Select(x => (int)x["id"]).ToArray();
		}

		public async Task<bool> DeleteFavorite(int id)
		{
			CheckFileExistence();
			var currentJsonData = GetJsonDataValues();

			try
			{
				currentJsonData["Data"] = JArray.FromObject(currentJsonData["Data"].Where(x => (int)x["id"] != id));
				//reorder Rank
				using (StreamWriter sw = new StreamWriter(Directory.GetCurrentDirectory() + "\\data." + _userName + ".json"))
				{
					sw.WriteLine(JsonConvert.SerializeObject(currentJsonData));
				}
				return true;
			}
			catch(Exception)
			{
				return false;
			}
		}

		public async Task<JArray> UpdateFavoriteRank(Dictionary<string, string> project, bool isUp)
		{
			CheckFileExistence();
			var currentJsonData = GetJsonDataValues();

			var dataJArray = JArray.FromObject(currentJsonData["Data"]);
			for (var i =0; i < dataJArray.Count; i++)
			{
				if ((string)dataJArray[i]["id"] == project["id"])
				{
					if (isUp)
					{
						dataJArray[i]["rank"] = (int.Parse(project["rank"]) - 1).ToString();
						dataJArray[i - 1]["rank"] = (int.Parse((string)dataJArray[i - 1]["rank"]) + 1).ToString();
					}
					else
					{
						dataJArray[i]["rank"] = (int.Parse(project["rank"]) + 1).ToString();
						dataJArray[i + 1]["rank"] = (int.Parse((string)dataJArray[i + 1]["rank"]) - 1).ToString();
					}
				}
			}

			currentJsonData["Data"] = JArray.FromObject(dataJArray.OrderBy(x => x["rank"]));

			try
			{
				using (StreamWriter sw = new StreamWriter(Directory.GetCurrentDirectory() + "\\data." + _userName + ".json"))
				{
					sw.WriteLine(JsonConvert.SerializeObject(currentJsonData));
				}
				return JArray.FromObject(currentJsonData["Data"]);
			}
			catch (Exception)
			{
				return new JArray();
			}
		}

		private JObject GetJsonDataValues()
		{
			using (StreamReader sr = new StreamReader(Directory.GetCurrentDirectory() + "\\data."+ _userName + ".json"))
			{ 
				var srValue = sr.ReadToEnd();
				if (!string.IsNullOrWhiteSpace(srValue))
				{
					return JObject.Parse(srValue);
				}
				else
				{
					return null;
				}
			}
		}

		private async void BuildDataFile()
		{

			if (!File.Exists(Directory.GetCurrentDirectory() + "\\data." + _userName + ".json"))
			{
				await File.AppendAllTextAsync(
					Directory.GetCurrentDirectory() + "\\data." + _userName + ".json", "{\"Data\":[]}");
			}
		}

		private void CheckFileExistence()
		{
			if (!File.Exists("\\data." + _userName + ".json"))
			{
				BuildDataFile();
			}
		}
	}
}
