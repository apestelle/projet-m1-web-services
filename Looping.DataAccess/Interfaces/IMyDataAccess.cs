﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Looping.DataAccess.Interfaces {
	public interface IMyDataAccess {
		Task<JArray> GetAll();
		Task<int[]> GetFavoriteIds();
		Task<Dictionary<string, string>> CreateFavorite(Dictionary<string, string> favorite);
		Task<bool> DeleteFavorite(int id);
		Task<JArray> UpdateFavoriteRank(Dictionary<string,string> project, bool isUp);

	}
}
