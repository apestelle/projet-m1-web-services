﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Looping.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Looping.WebApp.Controllers {
	[Route("api/[controller]")]
	[ApiController]
	public class HomeController : ControllerBase {
		private readonly IMyService _myService;

		public HomeController(IMyService myService) {
			_myService = myService;
		}

		[HttpGet("favorites")]
		public async Task<ActionResult<string>> GetAllFavorites()
		{
			var content = await _myService.GetAll();

			if (content.ToList().Count == 0)
				return NoContent();
			else
				return Ok(content);
		}

		[HttpGet("favoritesIds")]
		public async Task<ActionResult<int[]>> GetFavoriteIds()
		{
			var content = await _myService.GetFavoriteIds();

			if (content.ToList().Count == 0)
				return Ok(new Newtonsoft.Json.Linq.JArray());
			else
				return Ok(content);
		}


		[HttpPost("favorites/create")]
		public async Task<ActionResult<Dictionary<string, string>>> CreateFavorite()
		{
			var content = await _myService.CreateFavorite(HttpContext.Request.Form["object"]);
			
			if (content.ToList().Count == 0)
				return NoContent();
			else
				return Ok(content);
		}

		[HttpDelete("favorites/delete/{id}")]
		public async Task<ActionResult> DeleteFavorite(int id)
		{
			var result = await _myService.DeleteFavorite(id);

			if (result == true)
				return NoContent();
			else
				return Ok();
		}

		[HttpPut("favorites/updateFavoriteRank")]
		public async Task<ActionResult> UpdateFavoriteRank()
		{
			var result = await _myService.UpdateFavoriteRank(HttpContext.Request.Form["project"], bool.Parse(HttpContext.Request.Form["isUp"]));

			if (string.IsNullOrWhiteSpace(result))
				return NoContent();
			else
				return Ok(result);
		}
	}
}
