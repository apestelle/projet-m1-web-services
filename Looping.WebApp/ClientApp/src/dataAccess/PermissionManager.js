/**
 *  0 => No access
 * 10 => Guest access
 * 20 => Reporter access
 * 30 => Developer access
 * 40 => Maintainer access
 * 50 => Owner access # Only valid for groups
 */
import BaseManager from './BaseManager';

export default class PermissionManager extends BaseManager {
	static async getProjetMembers(projectId) {
		const res = await BaseManager.fetch('https://gitlab.com/api/v4/projects/' + projectId + '/members/all');

		return res.json();
	}

	static async getProjectMaintainers(projectId) {
		const members = await PermissionManager.getProjetMembers(projectId);

		return members.filter((elt) => parseInt(elt.access_level) >= 40);
	}
}
