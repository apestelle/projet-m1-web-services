export default class TokenManager {
	static #fetchTokenFromURL = () => {
		const url = window.location.href;
		const anchor = url.substring(url.indexOf("#") + 1);
		let token = anchor.split('&')[0].split('=')[1];

		if(token) {
			localStorage.token = token;
		}
	}

	static #fetchExpireTime = () => {
		localStorage.tokenExpiration = Date.now() + 600 * 1000; // @TODO
	}

	static #requestToken = () => {
		window.location.href = 'https://gitlab.com/oauth/authorize?client_id=' + '25b42db21f79133793c8d9b4a7b943e655d86e30c8691fa26f50d984552256b4' +
			'&redirect_uri=' + 'https://localhost:44315' +
			'&response_type=token&state=' + Date.now() + '-' + Math.random() +
			'&scope=' + 'openid+profile+email+read_user+api';
	}

	static fetchToken = () => {
		TokenManager.#fetchTokenFromURL();
		TokenManager.#fetchExpireTime();

		const expiration = parseInt(localStorage.tokenExpiration, 10);
		
		if(!localStorage.token || expiration <= Date.now()) {
			TokenManager.#requestToken();
		} else {
			setTimeout(() => {
				TokenManager.#requestToken();
			}, expiration - Date.now());
		}
	}

	static get token() {
		return localStorage.token;
	}
}