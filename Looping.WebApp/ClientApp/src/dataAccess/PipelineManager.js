import BaseManager from './BaseManager';

import PermissionManager from './PermissionManager';

export default class PipelineManager extends BaseManager {
	static async getProjectPipelineList(projectId) {
		if(!projectId) {
			return [];
		}

		let retour = [];
		let jsonData = [];

		let i = 1;
		do {
			const res = await BaseManager.fetch('https://gitlab.com/api/v4/projects/' + projectId + '/pipelines?per_page=100&page=' + i++);

			jsonData = await res.json();
			retour = [...retour, ...jsonData];
		} while(Array.isArray(jsonData) && jsonData.length > 0);

		return retour;
	}

	static async getPipelineForAllPrjects(projects) {
		const promises = [];

		for(let i = 0; i < projects.length; i++) {
			const id = i;

			promises.push(new Promise(async(resolve, reject) => {
				try {
					projects[id].pipeline_results = await PipelineManager.getProjectPipelineList(projects[id].id);
				} catch(e) {
					projects[id].pipeline_results = [];
				}

				if(projects[id].pipeline_results.length === 0) {
					projects[id].pipeline_percentage = 'N/A';
				} else {
					projects[id].pipeline_percentage = Math.round(
						100 * projects[id].pipeline_results.filter((elt) => elt.status === 'success').length / projects[id].pipeline_results.length
					) + '%';
				}

				resolve();
			}));

			promises.push(new Promise(async(resolve, reject) => {
				try {
					projects[id].maintainers = await PermissionManager.getProjectMaintainers(projects[id].id);
				} catch(e) {
					console.log(e);
					projects[id].maintainers = [];
				}

				resolve();
			}));
		}

		await Promise.all(promises);

		return projects;
	}

	static async runNewPipeline(projectId) {
		await BaseManager.fetch('https://gitlab.com/api/v4/projects/' + projectId + '/pipeline?ref=master', 'POST');
	}

	static async retryPipeline(projectId, pipelineId) {
		await BaseManager.fetch('https://gitlab.com/api/v4/projects/' + projectId + '/pipelines/' + pipelineId + '/retry', 'POST');
	}

	static async cancelPipeline(projectId, pipelineId) {
		await BaseManager.fetch('https://gitlab.com/api/v4/projects/' + projectId + '/pipelines/' + pipelineId + '/cancel', 'POST');
	}

	static async deletePipeline(projectId, pipelineId) {
		await BaseManager.fetch('https://gitlab.com/api/v4/projects/' + projectId + '/pipelines/' + pipelineId, 'DELETE');
	}
}
