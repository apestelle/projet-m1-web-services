// curl 'https://gitlab.com/api/graphql'
// --header "Authorization: Bearer $GRAPHQL_TOKEN"
// --header "Content-Type: application/json"
// --request POST
// --data "{\"query\": \"query {currentUser {name}}\"}"

import TokenManager from './TokenManager';

export default class UserManager {
	static async getUserData() {
		const res = await fetch('https://gitlab.com/api/graphql', {
			method: 'POST',
			body: JSON.stringify({query: "query {currentUser {name, username, avatarUrl}}"}),
			headers: {
				"Content-Type": "application/json",
				Authorization: 'Bearer ' + TokenManager.token,
			},
		});

		const json = await res.json();

		return json.data.currentUser;
	}
}
