import BaseManager from './BaseManager';

export default class SearchManager extends BaseManager {
	static async searchProject(searchTerms) {
		const res = await BaseManager.fetch('https://gitlab.com/api/v4/search?scope=projects&search=' + searchTerms);

		return res.json();
	}
}
