import TokenManager from './TokenManager';

export default class BaseManager {
	static fetch = async(url, method='GET', headers={}) => {
		return fetch(url, {
			method,
			headers: {
				Authorization: 'Bearer ' + TokenManager.token,
			},
		});
	}
}
