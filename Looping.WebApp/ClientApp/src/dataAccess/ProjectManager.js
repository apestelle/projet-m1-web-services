import BaseManager from './BaseManager';
import TokenManager from './TokenManager';

export default class ProjectManager extends BaseManager {
	static async getAllUserProjects(username) {
		let retour = [];
		let jsonData = [];

		let i = 1;
		do {
			const res = await BaseManager.fetch('https://gitlab.com/api/v4/users/' + username + '/projects?per_page=100&page=' + i++);

			jsonData = await res.json();
			retour = [...retour, ...jsonData];
		} while(Array.isArray(jsonData) && jsonData.length > 0);

		return retour;
	}

	static async getProject(id) {
		const res = await BaseManager.fetch('https://gitlab.com/api/v4/projects/' + id);

		return res.json();
	}

	static async getPublicProjects(page) {
		const res = await BaseManager.fetch('https://gitlab.com/api/v4/projects?per_page=25&page=' + page);

		return res.json();
	}

	static async editName(projectId, newName) {
		await fetch('https://gitlab.com/api/v4/projects/' + projectId, {
			method: 'PUT',
			body: JSON.stringify({name: newName}),
			headers: {
				"Content-Type": "application/json",
				Authorization: 'Bearer ' + TokenManager.token,
			},
		});
	}
}
