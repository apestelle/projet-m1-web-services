import BaseManager from './BaseManager';

export default class FavoriteManager extends BaseManager {
	static async getFavorites() {
		const res = await fetch('api/home/favorites');
		return res.json();
	}

	static async getFavoriteIds() {
		const res = await fetch('api/home/favoritesIds');
		return res.json();
	}

	static async createFavorite(project) {
		const object = {
			id: project.id,
			name: project.name,
			description: project.description,
			pipeline_percentage: project.pipeline_percentage,
			web_url: project.web_url,
		};

		const formData = new FormData();
		formData.append('object', JSON.stringify(object));

		await fetch('api/home/favorites/create', {
			method: 'post',
			body: formData,
		});
	}

	static async deleteFavorite(projectId) {
		await fetch('api/home/favorites/delete/' + projectId, {
			method: 'delete',
		});
	}

	static async updateFavoriteRank(project, isUp) {
		const formData = new FormData();
		formData.append('project', JSON.stringify(project));
		formData.append('isUp', JSON.stringify(isUp));

		const res = await fetch('api/home/favorites/updateFavoriteRank', {
			method: 'put',
			body: formData,
		});

		return res.json();
	}
}
