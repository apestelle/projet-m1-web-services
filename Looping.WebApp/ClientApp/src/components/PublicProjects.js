import BaseProject from './BaseProject';

import TokenManager from '../dataAccess/TokenManager';
import PipelineManager from '../dataAccess/PipelineManager';
import ProjectManager from '../dataAccess/ProjectManager';

export default class PublicProjects extends BaseProject {
	constructor(props) {
		super(props);

		this.state = {
			projects: [],
			favorites: [],
			page: 1,
		};
	}

	async componentDidMount() {
		TokenManager.fetchToken();

		await this.fetchApi();
		await this.getFavoriteIds();
	}

	async fetchApi() {
		let projects = await ProjectManager.getPublicProjects(this.state.page);
		projects = await PipelineManager.getPipelineForAllPrjects(projects);

		this.setState({
			projects,
		});
	}

	render() {
		return this.renderProjectList(this.state.projects);
	}
}
