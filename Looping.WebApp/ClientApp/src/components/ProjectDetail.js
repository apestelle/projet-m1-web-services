import React, { useState, useEffect } from 'react';

import { makeStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import InputBase from '@material-ui/core/InputBase';

import Grid from '@material-ui/core/Grid';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';

import ProjectManager from '../dataAccess/ProjectManager';
import PipelineManager from '../dataAccess/PipelineManager';

import {
	useParams
} from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
	root: {
		padding: '1em',
	},
}));

export default function ProjectDetail(props) {
	const { id } = useParams();
	const [project, setProject]= useState(null);
	const classes = useStyles();

	// Fetch
	useEffect(() => {
		async function fetchProject(projects, id) {
			let project = props.projects.filter((elt) => elt.id == id)[0];

			if(project) {
				setProject(project);
				return;
			}

			project = await ProjectManager.getProject(id);
			project.pipeline_results = await PipelineManager.getProjectPipelineList(project.id);

			setProject(project);
		}

		fetchProject(props.projects, id);
	}, [props.projects, id]);

	// Render
	if(!project) {
		return (<span>Loading ...</span>);
	}

	const isMaintainer = project.maintainers && project.maintainers.filter((elt) => elt.username === props.user.username).length > 0;

	const pipelinesList = [];
	for(const pipeline of project.pipeline_results) {
		const creationDate = new Date(pipeline.created_at);

		let deleteBtn = null;
		if(isMaintainer) {
			deleteBtn = (
				<Button onClick={async() => {
						await PipelineManager.deletePipeline(id, pipeline.id);
						window.location.reload(); // TODO: do it better
					}} color="secondary" align="right">
						<span role="img" aria-label="delete">🗑️</span>
				</Button>
			);

			if(pipeline.status === 'running') {
				 deleteBtn = (
				 	<Button onClick={async() => {
							await PipelineManager.cancelPipeline(id, pipeline.id);
							window.location.reload(); // TODO: do it better
						}} color="secondary" align="right">
							<span role="img" aria-label="cancel">X</span>
					</Button>
				);
			}
		}

		let retryBtn = null;
		if(isMaintainer) {
			retryBtn = (
				<Button onClick={async() => {
						await PipelineManager.retryPipeline(id, pipeline.id);
						window.location.reload(); // TODO: do it better
					}} color="secondary" align="right">
					<span role="img" aria-label="retry">↺</span>
				</Button>
			);
		}

		pipelinesList.push(
			<TableRow key={pipeline.id}>
				<TableCell>{creationDate.toLocaleDateString() + ' ' + creationDate.toLocaleTimeString()}</TableCell>
				<TableCell>{pipeline.status}</TableCell>
				<TableCell>
					{retryBtn}
					{deleteBtn}
				</TableCell>
			</TableRow>
		);
	}

	let newBtn = null;
	if(isMaintainer) {
		newBtn = (
			<Button onClick={async() => {
					await PipelineManager.runNewPipeline(id);
					window.location.reload(); // TODO: do it better
				}} color="secondary" align="right">
				<span role="img" aria-label="new">➕</span> Run new pipeline
			</Button>
		);
	}

	if(pipelinesList.length === 0) {
		pipelinesList.push(
			<TableRow key="none">
				<TableCell>No pipeline history</TableCell>
			</TableRow>
		);
	}

	return (
		<div className={classes.root}>
			<Grid container spacing={2}>
				<Grid item md={6}>
					<Typography align="left">
						<h1>Project</h1>
					</Typography>

					<div>
						<InputBase defaultValue={project.name} className="project-detail-name" /><Button bvariant="contained" color="primary" onClick={(e) => {
							const newName = e.target.parentNode.parentNode.children[0].children[0].value; // @TODO: faire mieux que ca ...

							if(newName === project.name) {
								return;
							}

							project.name = newName;

							ProjectManager.editName(project.id, newName)
						}}>Changer Nom</Button>
					</div>

					<p>{project.description}</p>
				</Grid>

				<Grid item md={6}>
					<Typography align="right">
						<h1>Pipelines</h1>
					</Typography>

					{newBtn}

					<Table>
						<TableBody>
							{pipelinesList}
						</TableBody>
					</Table>
				</Grid>
	    	</Grid>
	    </div>
    );
}
