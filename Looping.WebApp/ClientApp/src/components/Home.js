import BaseProject from './BaseProject';

import FavoriteManager from '../dataAccess/FavoriteManager';

export default class Home extends BaseProject {
	constructor(props) {
		super(props);

		this.state = {
			projects: [],
			favorites: [],
		};
	}

	async componentDidMount() {
		this.setState({
			projects: await FavoriteManager.getFavorites(),
		});

		this.getFavoriteIds();
	}

	render() {
		return this.renderProjectList(this.state.projects, true);
	}
}
