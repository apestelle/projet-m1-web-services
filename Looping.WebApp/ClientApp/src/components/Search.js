import React from 'react';
import { withRouter } from 'react-router-dom';

import BaseProject from './BaseProject';

import PipelineManager from '../dataAccess/PipelineManager';
import SearchManager from '../dataAccess/SearchManager';

export default withRouter(class Search extends BaseProject {
	async componentDidMount() {
		const searchTerms = this.props.match.params.searchTerms;

		let searchResults = await SearchManager.searchProject(searchTerms);
		searchResults = await PipelineManager.getPipelineForAllPrjects(searchResults);

		this.setState({
			searchResults,
		});
	}

	render() {
		if(!this.state.searchResults) {
			return (<span>Loading ...</span>);
		}

		return this.renderProjectList(this.state.searchResults);
	}
});
