import React, { Component } from 'react';

import Button from '@material-ui/core/Button';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Badge from '@material-ui/core/Badge';

import FavoriteManager from '../dataAccess/FavoriteManager';

export default class BaseProject extends Component {
	constructor(props) {
		super(props);

		this.state = {
			favorites: [],
		};
	}

	async componentDidMount() {
		this.getFavoriteIds();
	}

	async handleFavorite(project) {
		const element = document.getElementById(project.id);

		if (element.innerText === '☆') {
			await FavoriteManager.createFavorite(project);
			element.innerText = '★';

		} else {
			await FavoriteManager.deleteFavorite(project.id);
			element.innerText = '☆';
	    }
	}

	async getFavoriteIds() {
		this.setState({
			favorites: await FavoriteManager.getFavoriteIds(),
		});
    }

	async handleRank(project, isUp) {
		this.setState({
			projects: await FavoriteManager.updateFavoriteRank(project, isUp),
		});
	}

	renderProjectList(projects, displayRankArrows=false) {
		const list = [];
		for(const projectId in projects) {
			const project = projects[projectId];

			const html = this.state.favorites.includes(parseInt(project.id)) ? '★':'☆';

			let arrowUp = <Button onClick={() => this.handleRank(project, true)}>↑</Button>;
			let arrowDown = <Button onClick={() => this.handleRank(project, false)}>↓</Button>;

			if (!displayRankArrows || project.rank == 1) {
				arrowUp = <Button></Button>;
			}
			if (!displayRankArrows || project.rank == projects.length) {
				arrowDown = null;
			}

			list.push(
				<TableRow key={project.id}>
					<TableCell><a href={project.web_url}>{project.name}</a></TableCell>
					<TableCell>{project.description}</TableCell>
					<TableCell>{project.pipeline_percentage}</TableCell>
					<TableCell>
						<Button href={'/projectDetail/' + project.id} color="secondary">
							<span role="img" aria-label="info">🔍</span>
						</Button>
						<Button id={project.id} color="secondary" onClick={() => this.handleFavorite(project)}> {html}
						</Button>
					</TableCell>
					<TableCell>
						<Badge color="secondary" badgeContent={project.rank}>
						</Badge>
					</TableCell>
					<TableCell>
						{arrowUp}
						{arrowDown}
					</TableCell>
				</TableRow>
			);
		}

		let rankHeader = null;
		if(displayRankArrows) {
			rankHeader = <TableCell>Rank</TableCell>;
		}

		return (
			<Table>
				<TableHead>
					<TableRow>
						<TableCell>Nom</TableCell>
						<TableCell>Description</TableCell>
						<TableCell>Reussite Pipeline</TableCell>
						<TableCell></TableCell>
						{rankHeader}
						<TableCell></TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{list}
				</TableBody>
			</Table>
		);
	}
}
