import BaseProject from './BaseProject';

export default class Projects extends BaseProject {
	render() {
		return this.renderProjectList(this.props.projects);
	}
}
