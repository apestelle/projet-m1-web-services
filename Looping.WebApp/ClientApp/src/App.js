import React, { Component } from 'react';

import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';

import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import InputBase from '@material-ui/core/InputBase';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import Home from './components/Home';
import Projects from './components/Projects';
import ProjectDetail from './components/ProjectDetail';
import PublicProjects from './components/PublicProjects';
import Search from './components/Search';
import Stats from './components/Stats';

import TokenManager from './dataAccess/TokenManager';
import PipelineManager from './dataAccess/PipelineManager';
import ProjectManager from './dataAccess/ProjectManager';
import UserManager from './dataAccess/UserManager';

import {
		BrowserRouter as Router,
		Switch,
		Route,
} from 'react-router-dom';

const theme = createMuiTheme({
	palette: {
		primary: blue,
		secondary: {
			main: '#215223',
		},
	},
});

export default class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			user: {},
			projects: [],
		};
	}

	async componentDidMount() {
		TokenManager.fetchToken();

		const user = await UserManager.getUserData();
		let projects = await ProjectManager.getAllUserProjects(user.username);
		projects = await PipelineManager.getPipelineForAllPrjects(projects);

		this.setState({
			user,
			projects,
		});
	}

	render() {
		let avatar = null;
		if(this.state.user && this.state.user.avatarUrl) {
			avatar = (<img width="32" height="32" src={'https://gitlab.com' + this.state.user.avatarUrl} alt="avatar" />);
		}

		return (
			<Router>
				<ThemeProvider theme={theme}>
					<div>
						<AppBar position="static">
							<Toolbar>
								<Grid
									justify="space-between" // Add it here :)
									container
									spacing={10}
								>
									<Grid item>
									<Button href="/" color="secondary">
										Home
									</Button>
									<Button href="/projects" color="secondary">
										Projects
									</Button>
									<Button href="/publicProjects" color="secondary">
										Public Projects
									</Button>
									<Button href="/stats" color="secondary">
										Stats
									</Button>
									</Grid>
									<Grid item>
										<InputBase
											placeholder="Search project in Gitlab ..."
											style={{
												border: '1px solid white',
												padding: '0 0.5em',
												color: 'white',
											}}
											inputProps={{ 'aria-label': 'search' }}
											onKeyPress={(e) => {
												if(e.which === 13) {
													window.location.href = '/search/' + e.target.value;
												}
											}}
										/>
									</Grid>
									<Grid item>
										<Typography variant="h6" color="inherit" className="menu-userData" noWrap >
											{avatar}
											<span>{this.state.user.name}</span>
										</Typography>
									</Grid>
								</Grid>
							</Toolbar>
						</AppBar>

						<Switch>
							<Route exact path="/projects">
								<Projects projects={this.state.projects} />
							</Route>
							<Route exact path="/publicProjects">
								<PublicProjects />
							</Route>
							<Route exact path="/stats">
								<Stats />
							</Route>
							<Route exact path="/">
								<Home projects={this.state.projects} />
							</Route>
							<Route path="/projectDetail/:id">
								<ProjectDetail projects={this.state.projects} user={this.state.user} />
							</Route>
							<Route path="/search/:searchTerms">
								<Search user={this.state.user} />
							</Route>
						</Switch>
					</div>
				</ThemeProvider>
			</Router>
		);
	}
}
