using AspNet.Security.OAuth.GitLab;

using Looping.Business.Implementations;
using Looping.Business.Interfaces;
using Looping.Common;
using Looping.DataAccess.Implementations;
using Looping.DataAccess.Interfaces;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Looping.WebApp {
	public class Startup {
		public Startup(IConfiguration configuration) {
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services) {
			var appSettingsSection = Configuration.GetSection("AppSettings");
			services.AddSingleton(Configuration);
			services.Configure<AppSettings>(appSettingsSection);

			// DataAccess
			services.AddTransient<IMyDataAccess, MyDataAccess>();

			// Services
			services.AddHttpContextAccessor();
			services.AddTransient<IMyService, MyService>();

			services.AddAuthentication(options => {
				options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
				options.DefaultChallengeScheme = GitLabAuthenticationDefaults.AuthenticationScheme;
			})
			.AddCookie()
			.AddGitLab(options => {
				options.ClientId = "25b42db21f79133793c8d9b4a7b943e655d86e30c8691fa26f50d984552256b4";
				options.ClientSecret = "76ba5bcc432c49411788e98e29201c80effad158e0e855bf9d00cece83db6d6e";

				options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
				options.SaveTokens = true;
			});

			services.AddAuthorization(options => {
				var policy = new AuthorizationPolicyBuilder()
								.RequireAuthenticatedUser()
								.Build();
				options.AddPolicy("auth", policy);
			});

			/**
             * MVC
             */
			services.AddMvc();

			// In production, the React files will be served from this directory
			services.AddSpaStaticFiles(configuration => {
				configuration.RootPath = "ClientApp/build";
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
			if (env.IsDevelopment()) {
				app.UseDeveloperExceptionPage();

			} else {
				app.UseExceptionHandler("/Error");
				app.UseHsts();
			}

			app.UseHttpsRedirection();

			app.UseStaticFiles();
			app.UseSpaStaticFiles();

			app.UseRouting();

			app.UseAuthentication();
			app.UseAuthorization();

			app.Use(async (context, next) => {
				if (context.User == null || !context.User.Identity.IsAuthenticated) {
					await context.ChallengeAsync();
				} else {
					await next();
				}
			});

			app.UseEndpoints(endpoints => {
				endpoints.MapControllerRoute(
					name: "defaultHome",
					pattern: "{controller}/{action=Test}/{id?}");
			});

			app.UseSpa(spa => {
				spa.Options.SourcePath = "ClientApp";

				if (env.IsDevelopment()) {
					spa.UseReactDevelopmentServer(npmScript: "start");
				}
			});
		}
	}
}

