﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Looping.Business.Interfaces {
	public interface IMyService {
		Task<string> GetAll();
		Task<Dictionary<string, string>> CreateFavorite(string json);
		Task<int[]> GetFavoriteIds();
		Task<bool> DeleteFavorite(int id);
		Task<string> UpdateFavoriteRank(string json, bool isUp);

	}
}
