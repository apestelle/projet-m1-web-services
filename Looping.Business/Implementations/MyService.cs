﻿using Looping.Business.Interfaces;
using Looping.DataAccess.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Looping.Business.Implementations {
	public class MyService : IMyService {
		private IMyDataAccess _dataAccess;
		public MyService(IMyDataAccess dataAccess)
		{
			_dataAccess = dataAccess;
		}

		public async Task<string> GetAll()
		{
			try
			{
				return JsonConvert.SerializeObject(await _dataAccess.GetAll());
			}
			catch (Exception)
			{
				return string.Empty;
			}
		}

		public async Task<int[]> GetFavoriteIds()
		{
			try
			{
				return await _dataAccess.GetFavoriteIds();
			}
			catch (Exception)
			{
				return null;
			}
		}

		public async Task<Dictionary<string, string>> CreateFavorite(string json)
		{
			try
			{
				var favorite = JsonConvert.DeserializeObject<Dictionary<string,string>>(json);
				return await _dataAccess.CreateFavorite(favorite);
			}
			catch(Exception)
			{
				return null;
			}
		}


		public async Task<bool> DeleteFavorite(int id)
		{
			try
			{
				return await _dataAccess.DeleteFavorite(id);
			}
			catch (Exception)
			{
				return false;
			}
		}

		public async Task<string> UpdateFavoriteRank(string json, bool isUp)
		{
			try
			{
				var project = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
				return JsonConvert.SerializeObject(await _dataAccess.UpdateFavoriteRank(project, isUp));
			}
			catch (Exception)
			{
				return String.Empty;
			}
		}
	}
}
